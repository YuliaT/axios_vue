import Vue from 'vue'
import App from './App.vue'

import axios from 'axios'
import VueAxios from 'vue-axios'

import {BootstrapVue} from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import VueRouter from 'vue-router'

import Index from './pages/index'

Vue.use(BootstrapVue)
Vue.use(VueAxios, axios)
Vue.use(VueRouter)

Vue.config.productionTip = false

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Index,
            name: 'Index',
            children: [
                {
                    path: '/todos',
                    component: () => import('./pages/todos'),
                    name: 'todos',
                },
                {
                    path: '/todos/:id',
                    component: () => import('./pages/todos-id'),
                    name: 'todos-id'
                }

            ]
        },
        {
            path: '/users',
            component: () => import('./pages/Users'),
            name: 'users',
            children: [
                {
                    path: '/:id',
                    component: () => import('./pages/User'),
                    name: 'users-id'
                }
            ]
        },


    ]
})


new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
